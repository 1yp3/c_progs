#include<stdio.h>

int front = 0,rear = -1;

void push(int array[50],int item) {
  if ( rear == 50 ) {
    printf("Queue full.\n");
  }
  else {
    array[++rear] = item;
    printf("%d pushed.\n",item);
  }
}

int pop(int array[50]) {
  if ( rear < front ) {
    printf("Queue empty.\n");
    return 0;
  }
  else {
    int temp = array[front];
    int i;
    for(i=front;i<rear;i++) {
      array[i]=array[i+1];
    }
    rear--;
    return temp;
  }
}

void display(int array[50]) {
  int i;
  for(i=front; i<=rear; i++) {printf("%d\t",array[i]);}
  printf("\n");
}

void main() {
  int array[50],item,choice;
  while(1) {
    printf("1. Push\n2. Pop\n3. Display queue\n\t: ");
    scanf("%d",&choice);
    if ( choice == 1) {
      printf("Enter the element:");
      scanf("%d",&item);
      push(array,item);
    }
    else if ( choice == 2 ) {
      printf("%d popped.\n",pop(array));
    }
    else if( choice == 3) {display(array);}
    else {printf("Invalid Choice\n");}
    printf("Continue? (0: Abort. 1: Continue):");
    scanf("%d",&choice);
    if ( choice == 0 ) {break;}
  }
}
