#include<stdio.h>
#define TABLESIZE 100
char hashTable[TABLESIZE];

int hashFunc(char keyVal) {
	int index = ( keyVal % 29) + 1;
	return index;
}

int main() {
	int k;
	for(k=0; k<TABLESIZE; k++) hashTable[k]=-1;
	char keyVal[75];
	int i=0, controlVar = 0, controlVarJ, index, keyLen;
	printf( "Enter the Key Value: " );
	scanf( "%s", keyVal );
	for( keyLen = 0; keyVal[keyLen] != '\0'; keyLen++ );
	for( controlVar = 0; controlVar < keyLen; controlVar ++ ) {
		index = hashFunc( keyVal[controlVar] );
		if( hashTable[index] <= 0 ) {
			hashTable[index] = keyVal[controlVar];
		}
		else {
			controlVarJ = index + 1;
			while( controlVarJ != index ) {
				if( hashTable[controlVarJ] <= 0 ) {
					hashTable[controlVarJ] = keyVal[controlVar];
					break;
				}

				if( controlVarJ == TABLESIZE-1 ) {
					controlVarJ = 0;
				}
				else {
					controlVarJ ++;
				}
			}
		}
	}
	printf("Index\tValue\n");
	for( controlVar = 0; controlVar<TABLESIZE; controlVar++) {
		if( hashTable[controlVar] == -1 ) {
			printf("%d\n", controlVar);
		}
		else {
			printf("%d\t%c\n", controlVar, hashTable[controlVar]);
		}
	}
	printf( "\n" );
	return 0;
}
