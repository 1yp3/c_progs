#include <stdio.h>
#include <stdlib.h>

struct node {
  int data;
  struct node *prev;
  struct node *next;
};

struct node *head = NULL;

void insertFront(int data) {
  struct node *temp = (struct node*) malloc(sizeof(struct node));
  temp->data = data;
  temp->next = head->next;
  head->next = temp;
  temp->prev = head;
}

void insertEnd(int data) {
    struct node *temp = head;
    while( temp->next != NULL ) {
        temp = temp->next;
    }
    struct node *temp2 = (struct node*) malloc(sizeof(struct node));
    temp2->data = data;
    temp2->next = NULL;
    temp->next = temp2;
    temp2->prev = temp;
}

void insertAfter(int data, int pos) {
    struct node *temp = head;
    while( temp->data != pos ) {
        temp = temp->next;
    }
    struct node *newNode = (struct node*) malloc(sizeof(struct node));
    newNode->data = data;
    newNode->next = temp->next;
    temp->next = newNode;
    newNode->prev = temp;
    if ( newNode->next != NULL ) {
      newNode->next->prev = newNode;
    }

}

void delete(int pos) {
    struct node *temp = head;
    struct node *prev = NULL;
    while( temp->data != pos ) {
        prev = temp;
        temp = temp->next;
    }
    prev->next = temp->next;
    temp->next->prev = prev;
}

void display() {
  struct node *temp = head->next;
  printf("\n");
  while(temp != NULL) {
    printf("%d\n", temp->data);
    temp = temp->next;
  }
  printf("\n");
}

int main(int argc, char const *argv[]) {
  head = ( struct node* ) malloc(sizeof(struct node));
  head->next = NULL;
  head->prev = NULL;

  int choice, data, pos;
  do {
    printf("1. Insert at Front\n2. Insert After _\n3. Insert at End\n4. Delete a Node\n5. Display\n\n: ");
    scanf("%d", &choice);
    switch (choice) {
      case 1:
        printf("Enter the data: ");
        scanf("%d", &data);
        insertFront(data);
        break;
      case 2:
        printf("Enter the position: ");
        scanf("%d", &pos);
        printf("Enter the data: ");
        scanf("%d", &data);
        insertAfter(data, pos);
        break;
      case 3:
        printf("Enter the data: ");
        scanf("%d", &data);
        insertEnd(data);
        break;
      case 4:
        printf("Enter the position: ");
        scanf("%d", &pos);
        delete(pos);
        break;
      case 5:
        display();
        break;
      case 6:
        printf("Exiting..");
        break;
      default:
        printf("Invalid Choice.\n");
        break;
    }
  } while(choice != 6);
  return 0;
}
