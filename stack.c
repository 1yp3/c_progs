#include<stdio.h>

int top = -1;

void push(int array[50],int item) {
  if ( top == 50 ) {
    printf("Stack overflow.");
  }
  else {
    array[++top] = item;
    printf("%d pushed.\n",item);
  }
}

int pop(int array[50]) {
  if ( top == -1 ) {
    printf("Stack empty.");
    return 0;
  }
  else {
    return array[top--];
  }
}

void display(int array[50]) {
  int i=0;
  for(i=0; i<=top; i++) {printf("%d\t",array[i]);}
  printf("\n");
}

void main() {
  int array[50],item,choice;
  while(1) {
    printf("1. Push\n2. Pop\n3. Display stack\n\t: ");
    scanf("%d",&choice);
    if ( choice == 1) {
      printf("Enter the element:");
      scanf("%d",&item);
      push(array,item);
    }
    else if ( choice == 2 ) {
      printf("%d popped.\n",pop(array));
    }
    else if( choice == 3) {display(array);}
    else {printf("Invalid Choice\n");}
    printf("Continue? (0: Abort. 1: Continue):");
    scanf("%d",&choice);
    if ( choice == 0 ) {break;}
  }
}
